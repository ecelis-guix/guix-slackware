#!/bin/sh
#
#     Guix Slackware build script
#     Copyright (C) 2014 Ernesto Angel Celis de la Fuente
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Create Guix's build group and users
groupadd guix-builder
for i in `seq 1 10`;
do
  useradd -g guix-builder -G guix-builder \
  -d /var/empty -s `which nologin` \
  -c "Guix build user $i" --system \
  guix-builder$i;
done
# Create Guix required directories
mkdir -p /gnu/store
chgrp -R guix-builder /gnu/store
chmod 1775 /gnu/store
mkdir -p /var/guix/profiles/per-user
# Extra intructions post-install
echo "Post install steps..."
