GNU Guix for Slackware
======================

GNU Guix build scripts and related files for Slackware package creation.

Install
-------

In the following instrucions don't copy & paste blindly, add
ARCH=x86_64 if you are on Slackware64
 
    git clone https://gitorious.org/ecelis-guix/guix-slackware.git
    cd guix-slackware
    wget http://alpha.gnu.org/gnu/guix/guix-0.7.tar.gz
    su
    ARCH=x86_64./guix.SlackBuild # remove ARCH if 32bit system
    upgradepkg --install-new /tmp/guix-0.7-x86_64-1_cfe.tgz

In order to install packages as as a regular user you must create the
user's directory under /var/guix/profiles/per-user/$USER. Don't copy
paste, change $USER for the desired user or users

    su -
    mkdir /var/guix/profiles/per-user/$USER
    chown -R $USER:$USER /var/guix/profiles/per-user/$USER

Run
---

By default guix-daemon will be started at boot time, you can also
restart, stop and start it manually

    su -
    /etc/rc.d/rc.guixd start|stop|restart

As a regular user you mus add ~/.guix-profile to your $PATH

    echo "export PATH=$PATH:~/.guix-profile/bin" >> ~/.bashrc
    . ~/.bashrc
    guix --help

References
----------

  * [GNU Guix web site](http://www.gnu.org/software/guix/)
  * [Manual](http://www.gnu.org/software/guix/manual/)

All source code in this git repository is released under the GNU General
Public License version 3 terms. All guides and documents are released under
the GNU Free Documentation License version 1.3 terms.

2014, Ernesto Angel Celis de la Fuente
